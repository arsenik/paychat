package me.paychat.presenter;

import android.content.Context;

import me.paychat.view.LoginView;

/**
 * Created by a.troshin on 05.07.16.
 */
public class LoginPresenter extends BasePresenter<LoginView> {
    public void login(final Context context, final String phone) {
        getView().next();
    }
}
