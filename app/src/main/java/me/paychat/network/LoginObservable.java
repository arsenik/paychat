package me.paychat.network;

import me.paychat.model.UserInfo;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by a.troshin on 04.07.16.
 */
//TODO just for test and experimnt, will use Retrofit
public class LoginObservable extends Observable<UserInfo> {

    protected LoginObservable(OnSubscribe<UserInfo> f) {
        super(f);
    }

    public static LoginObservable login() {

        return new LoginObservable(new OnSubscribe<UserInfo>() {
            @Override
            public void call(Subscriber<? super UserInfo> subscriber) {
                //TODO handle login
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                subscriber.onNext(new UserInfo());
                subscriber.onCompleted();
            }
        });
    }
}
