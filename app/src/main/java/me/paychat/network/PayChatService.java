package me.paychat.network;

import me.paychat.model.Chat;
import me.paychat.model.CreateDialog;
import me.paychat.model.UserInfo;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by a.troshin on 04.07.16.
 */
public interface PayChatService {

    @GET("users/getByPhoneNumber")
    Observable<Response<UserInfo>> getByPhoneNumber(@Query("phone_number") String phone);

    @GET("transactions/getForDialogByID")
    Observable<Response<Chat>> getForDialogByID(@Query("dialog_id") long dialogId);

    @FormUrlEncoded
    @POST("users/create")
    Observable<Response<UserInfo>> usersCreate(@Field("phone_number") String phone);

//    @FormUrlEncoded
//    @POST("dialogs/createForUsers")
//    Observable<Response<CreateDialog>> createForUsers(@Field("phone_number") String phones);

    @FormUrlEncoded
    @POST("dialogs/createForUsers")
    Observable<Response<CreateDialog>> createForUsers(@Field("phone_number") String myPhone, @Field("phone_number") String contactPhone);
}
