package me.paychat.network;

import com.google.gson.Gson;

import android.util.Log;

import java.io.IOException;

import me.paychat.model.Chat;
import me.paychat.model.Transaction;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.ws.WebSocket;
import okhttp3.ws.WebSocketCall;
import okhttp3.ws.WebSocketListener;
import okio.Buffer;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by a.troshin on 04.07.16.
 */
public class SocketObservable extends Observable<Chat> {

    protected SocketObservable(OnSubscribe<Chat> f) {
        super(f);
    }

    public static SocketObservable listen2() {
        return new SocketObservable(new OnSubscribe<Chat>() {
            @Override
            public void call(Subscriber<? super Chat> subscriber) {

            }
        });
    }
}
