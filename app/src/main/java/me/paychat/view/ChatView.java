package me.paychat.view;

import java.util.List;

import me.paychat.model.Transaction;

/**
 * Created by a.troshin on 05.07.16.
 */
public interface ChatView extends IView {
    void enableSendForm();
    void disableSendForm();
    void addItem(Transaction transaction);
    void setItems(List<Transaction> transactions);
    void clearList();
    void clearEdit();
    void setChatTitle(String title);
    void showBalance(long balance);
    void startPaymentApp();
    void startPaymentApp(long userId);
}
