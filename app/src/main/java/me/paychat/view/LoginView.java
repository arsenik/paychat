package me.paychat.view;

/**
 * Created by a.troshin on 04.07.16.
 */
public interface LoginView extends IView {
    void showProgress();
    void hideProgress();
    void next();
    void showError(String message);
}
