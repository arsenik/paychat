package me.paychat.view;

import java.util.List;

import me.paychat.model.DialogInfo;

/**
 * Created by a.troshin on 04.07.16.
 */
public interface DialogsView extends IView {
    void showContactPicker();
    void showDialog(long dialogId);
    void showDialogs(List<DialogInfo> dialogInfos);
    void showLogin();
    void showProgress();
    void hideProgress();
}
