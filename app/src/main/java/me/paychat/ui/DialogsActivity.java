package me.paychat.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.paychat.R;
import me.paychat.model.DialogInfo;
import me.paychat.presenter.DialogsPresenter;
import me.paychat.util.ContactsUtil;
import me.paychat.util.UserUtil;
import me.paychat.view.DialogsView;

/**
 * Created by a.troshin on 04.07.16.
 */
public class DialogsActivity extends AppCompatActivity implements DialogsView {

    static final int PICK_CONTACT = 101;

    private DialogsPresenter mDialogsPresenter;

    private RecyclerView.LayoutManager mLayoutManager;

    private DialogsAdapter mDialogsAdapter;

    @BindView(R.id.create_dialog_button)
    FloatingActionButton mCreateDialogButton;

    @BindView(R.id.dialogs_list)
    RecyclerView mDialogsList;

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dialogs);
        ButterKnife.bind(this);
        mDialogsPresenter = new DialogsPresenter();
        mDialogsPresenter.attachView(this);
        mLayoutManager = new LinearLayoutManager(this);
        mCreateDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showContactPicker();
            }
        });
        mDialogsList.setLayoutManager(mLayoutManager);
        mDialogsList.setHasFixedSize(true);
        mDialogsAdapter = new DialogsAdapter(mDialogItemClickListener);
        mDialogsList.setAdapter(mDialogsAdapter);
        mDialogsPresenter.loadDialogs(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_dialogs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                mDialogsPresenter.logout(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private OnDialogItemClickListener mDialogItemClickListener = new OnDialogItemClickListener() {
        @Override
        public void onItemClick(DialogInfo dialogInfo) {
            showDialog(dialogInfo.id);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDialogsPresenter.detachView();
    }

    @Override
    public void showContactPicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void showDialog(long dialogId) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.DIALOG_ID, dialogId);
        startActivity(intent);
    }

    @Override
    public void showDialogs(List<DialogInfo> dialogInfos) {
        mDialogsAdapter.setItems(dialogInfos);
        mDialogsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
        mDialogsList.setVisibility(View.GONE);
        mCreateDialogButton.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
        mDialogsList.setVisibility(View.VISIBLE);
        mCreateDialogButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_CONTACT:
                String phone = ContactsUtil.getPhone(this, data);
                if (!TextUtils.isEmpty(phone)) {
                    mDialogsPresenter.createDialog(this, UserUtil.currentUserPhone(this), phone);
                }
                break;
            default:
                break;
        }
    }
}
