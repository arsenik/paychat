package me.paychat.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.paychat.R;
import me.paychat.model.Transaction;
import me.paychat.util.UserUtil;

/**
 * Created by a.troshin on 06.07.16.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {

    private ArrayList<Transaction> mTransactions;

    private Context mContext;

    private static final int MY_MESSAGE_VIEW_TYPE = 1;

    private static final int OTHER_MESSAGE_VIEW_TYPE = 2;

    OnChatItemClickListener mItemClickListener;

    public ChatAdapter(Context context, OnChatItemClickListener itemClickListener) {
        mContext = context;
        mTransactions = new ArrayList<>();
        mItemClickListener = itemClickListener;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                viewType == MY_MESSAGE_VIEW_TYPE ? R.layout.chat_item_right
                        : R.layout.chat_item_left, parent, false);
        ChatViewHolder chatViewHolder = new ChatViewHolder(view);
        return chatViewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (mTransactions.get(position).initialID == UserUtil.currentUserId(mContext)) {
            return MY_MESSAGE_VIEW_TYPE;
        }
        return OTHER_MESSAGE_VIEW_TYPE;
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClick(mTransactions.get(position));
            }
        });
        holder.bindView(mTransactions.get(position), position);
    }

    public void addItem(Transaction transaction) {
        mTransactions.add(0, transaction);
    }

    public void setItems(List<Transaction> transactions) {
        mTransactions.addAll(transactions);
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }

    public void clear() {
        mTransactions.clear();
    }
}
