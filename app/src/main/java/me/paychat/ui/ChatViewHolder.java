package me.paychat.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.Currency;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.paychat.R;
import me.paychat.model.Transaction;

/**
 * Created by a.troshin on 06.07.16.
 */
public class ChatViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.author)
    TextView author;

    @BindView(R.id.currency)
    TextView currency;

    public ChatViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(Transaction transaction, int position ) {
        String currencyTitle = Currency.getInstance("RUB").getSymbol();
        title.setText(String.valueOf(transaction.sum));
        author.setText(String.valueOf(transaction.initialID));
        currency.setText(currencyTitle);
    }
}
