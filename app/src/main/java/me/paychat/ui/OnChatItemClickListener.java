package me.paychat.ui;

import me.paychat.model.Transaction;

/**
 * Created by a.kazin on 08.07.16.
 */
public interface OnChatItemClickListener {
    void onItemClick(Transaction transaction);
}
