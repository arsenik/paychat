package me.paychat.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.paychat.R;
import me.paychat.presenter.LoginPresenter;
import me.paychat.view.LoginView;

/**
 * Created by a.troshin on 04.07.16.
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.button_login)
    Button mLogin;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    @BindView(R.id.main_layout)
    RelativeLayout mMainLayout;

    @BindView(R.id.phone)
    EditText mPhone;

    LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginPresenter = new LoginPresenter();
        mLoginPresenter.attachView(this);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = mPhone.getText().toString();
                mLoginPresenter.login(v.getContext(), phone);
            }
        });

        mLoginPresenter.checkLoggedIn(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.detachView();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mMainLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
        mMainLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void next() {
        Intent intent = new Intent(this, DialogsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showError(String message) {

    }
}
