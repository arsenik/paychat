package me.paychat.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import me.paychat.R;
import me.paychat.model.DialogInfo;

/**
 * Created by a.troshin on 05.07.16.
 */
public class DialogsAdapter extends RecyclerView.Adapter<DialogViewHolder> {

    private List<DialogInfo> mDialogInfos;
    private OnDialogItemClickListener mItemClickListener;

    public DialogsAdapter(OnDialogItemClickListener listener) {
        mDialogInfos = new LinkedList<>();
        mItemClickListener = listener;
    }

    public void setItems(List<DialogInfo> dialogInfoList) {
        mDialogInfos = new LinkedList<>();
        mDialogInfos = dialogInfoList;
    }

    @Override
    public DialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog, parent, false);
        DialogViewHolder dialogViewHolder = new DialogViewHolder(view);
        return dialogViewHolder;
    }

    @Override
    public void onBindViewHolder(DialogViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClick(mDialogInfos.get(position));
            }
        });
        holder.bindView(mDialogInfos.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mDialogInfos.size();
    }

}
