package me.paychat.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Currency;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.paychat.R;
import me.paychat.model.Transaction;
import me.paychat.presenter.ChatPresenter;
import me.paychat.util.UserUtil;
import me.paychat.view.ChatView;

/**
 * Created by a.troshin on 05.07.16.
 */
public class ChatActivity extends AppCompatActivity implements ChatView {

    public static final String DIALOG_ID = "dialogId";

    @BindView(R.id.send_button)
    Button mSendButton;

    @BindView(R.id.sum_edit)
    EditText mSumEdit;

    @BindView(R.id.chat_list)
    RecyclerView mChatList;


    private ChatPresenter mChatPresenter;

    private Handler mHandler = new Handler();

    private ChatAdapter mChatAdapter;

    private LinearLayoutManager mLayoutManager;

    private long mDialogId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        mChatPresenter = new ChatPresenter(this);
        mChatPresenter.attachView(this);

        mDialogId = getIntent().getLongExtra(DIALOG_ID, 0);

        mChatAdapter = new ChatAdapter(this, new OnChatItemClickListener() {
            @Override
            public void onItemClick(Transaction transaction) {
                //TODO for transaction phone
            }
        });
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mChatList.setLayoutManager(mLayoutManager);
        mChatList.setAdapter(mChatAdapter);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChatPresenter.sendMessage(UserUtil.currentUserId(v.getContext()),
                        mSumEdit.getText().toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_chat, menu);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.balance_view);

        setChatTitle("Вам должны: ");
        mChatPresenter.listen(mDialogId);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_payment:
                startPaymentApp();
//                mChatPresenter.payment(this, mDialogId, mChatPresenter.getBalance());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("socketFix", "onRestart");
        mChatPresenter.listen(mDialogId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("socketFix", "onStop");
        mChatPresenter.closeSocket();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChatPresenter.detachView();
    }

    @Override
    public void enableSendForm() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mSumEdit.setEnabled(true);
                mSendButton.setEnabled(true);
            }
        });
    }

    @Override
    public void disableSendForm() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mSumEdit.setEnabled(false);
                mSendButton.setEnabled(false);
            }
        });
    }

    @Override
    public void addItem(Transaction transaction) {
        mChatAdapter.addItem(transaction);
        mChatAdapter.notifyDataSetChanged();
    }

    @Override
    public void setItems(List<Transaction> transactions) {
        mChatAdapter.setItems(transactions);
        mChatAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        mChatAdapter.clear();
        mChatAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearEdit() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mSumEdit.getText().clear();
            }
        });
    }

    @Override
    public void setChatTitle(String title) {
        TextView chatTitle = (TextView) getSupportActionBar().getCustomView()
                .findViewById(R.id.title);
        if (chatTitle != null) {
            chatTitle.setText(title);
        }
    }

    @Override
    public void showBalance(final long balance) {
        if (getSupportActionBar() == null) {
            return;
        }
        String currencyTitle = Currency.getInstance("RUB").getSymbol();
        TextView balanceTv = (TextView) getSupportActionBar().getCustomView()
                .findViewById(R.id.balance);
        TextView currencyTv = (TextView) getSupportActionBar().getCustomView()
                .findViewById(R.id.currency);
        currencyTv.setText(currencyTitle);
        currencyTv.setTextColor(getResources().getColor(android.R.color.white));
        balanceTv.setText(String.valueOf(balance));
    }

    @Override
    public void startPaymentApp() {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(
                "qiwi://payment/form.action?provider=99&account=" + "79168626215" + "&amount=1"));
        startActivity(intent);
    }

    @Override
    public void startPaymentApp(long account) {
        //TODO deep link to phone number, get by initalID from userList
//        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("qiwi://payment/form.action?provider=99&account="+account+"&amount=" + mChatPresenter.getBalance()));
//        startActivity(intent);
    }
}
