package me.paychat.ui;

import me.paychat.model.DialogInfo;

/**
 * Created by a.troshin on 05.07.16.
 */
public interface OnDialogItemClickListener {
    void onItemClick(DialogInfo dialogInfo);
}
