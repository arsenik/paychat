package me.paychat.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.paychat.R;
import me.paychat.model.DialogInfo;

/**
 * Created by a.troshin on 05.07.16.
 */
public class DialogViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title)
    TextView title;

    public DialogViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(DialogInfo dialogInfo, int position) {

        title.setText("PayChat № "+String.valueOf(dialogInfo.id).substring(0,2));
    }
}
