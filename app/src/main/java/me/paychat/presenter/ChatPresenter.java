package me.paychat.presenter;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import me.paychat.model.Chat;
import me.paychat.model.Transaction;
import me.paychat.network.PayChatApi;
import me.paychat.util.UserUtil;
import me.paychat.view.ChatView;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.ws.WebSocket;
import okhttp3.ws.WebSocketCall;
import okhttp3.ws.WebSocketListener;
import okio.Buffer;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a.troshin on 05.07.16.
 */
public class ChatPresenter extends BasePresenter<ChatView> {

    public static final String WEBSOCKET_ENDPOINT = "ws://104.131.21.92:8080";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private final Executor writeExecutor = Executors.newSingleThreadExecutor();

    public WebSocket mWebSocket;

    private Subscription mSocketSubscription;

    private Subscription mRestSubscription;

    private long mBalance = 0;

    private Context mContext;

    private HashMap<Long, Long> mBalances;

    public ChatPresenter(Context context) {
        mContext = context;
        mBalances = new HashMap<>();
    }

    @Override
    public void attachView(ChatView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSocketSubscription != null) {
            mSocketSubscription.unsubscribe();
        }
        if (mRestSubscription != null) {
            mRestSubscription.unsubscribe();
        }
    }

    public long getBalance(long userId) {
        Long balance = mBalances.get(userId);
        if (balance != null) {
            return mBalances.get(userId);
        }
        return 0;
    }

    private long calcBalance(List<Transaction> transactions) {
        long balance = 0;
        for (Transaction transaction : transactions) {
            if (transaction.initialID == UserUtil.currentUserId(mContext)) {
                balance += transaction.sum;
            } else {
                balance -= transaction.sum;
            }
            long userBalance = getBalance(transaction.initialID);
            mBalances.put(transaction.initialID, transaction.sum + userBalance);
        }
        return balance;
    }

    public void listen(long dialogId) {
        Log.d("showBalance", "listen");
        getView().clearList();
        mRestSubscription = PayChatApi.api().getForDialogByID(dialogId)
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<retrofit2.Response<Chat>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(retrofit2.Response<Chat> chatResponse) {
                        Chat chat = chatResponse.body();
                        List<Transaction> transactions = chat.response;
                        Collections.reverse(transactions);
                        getView().setItems(transactions);
                        mBalance = calcBalance(transactions);
                        Log.d("showBalance", "balance: " + String.valueOf(mBalance));
                        getView().showBalance(mBalance);
                    }
                });

        mSocketSubscription = listenTransactions(dialogId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Transaction>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Transaction transaction) {
                        Log.d("rxChat", "id: " + transaction.id + " sum: " + transaction.sum);
                        getView().addItem(transaction);
                        mBalance += transaction.sum;
                        getView().showBalance(mBalance);
                    }
                });
    }

    public long getBalance() {
        return mBalance;
    }

    public void payment(Context context, long dialogId, long sum) {
        //TODO send request for payment on Paychat server
    }

    private Observable<Transaction> listenTransactions(final long dialogId) {
        return Observable.create(new Observable.OnSubscribe<Transaction>() {
            @Override
            public void call(final Subscriber<? super Transaction> subscriber) {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                        .readTimeout(2, TimeUnit.MINUTES).connectTimeout(2, TimeUnit.MINUTES)
                        .writeTimeout(2, TimeUnit.MINUTES);
                Request request = new Request.Builder()
                        .url(WEBSOCKET_ENDPOINT + "/dialog/" + dialogId).build();
                WebSocketCall call = WebSocketCall.create(httpClient.build(), request);
                call.enqueue(new WebSocketListener() {
                    @Override
                    public void onOpen(WebSocket webSocket, Response response) {
                        mWebSocket = webSocket;
                        getView().enableSendForm();
//                        subscriber.onCompleted();
                    }

                    @Override
                    public void onFailure(IOException e, Response response) {
//                        mWebSocket = null;
                        subscriber.onError(e);
//                        subscriber.onCompleted();
                    }

                    @Override
                    public void onMessage(ResponseBody message) throws IOException {
                        String msg = message.string();
                        Log.d("websocketLog", "[onMessage] " + msg);
                        Transaction transaction = new Gson().fromJson(msg, Transaction.class);
                        message.close();
                        subscriber.onNext(transaction);
//                        subscriber.onCompleted();
                    }

                    @Override
                    public void onPong(Buffer payload) {
                        Log.d("websocketLog", "[onPong] " + payload.readUtf8());
                    }

                    @Override
                    public void onClose(int code, String reason) {
                        mWebSocket = null;
                        Log.d("websocketLog", "[onClose] code: " + code + " reason: " + reason);
//                        getView().disableSendForm();
                        subscriber.onCompleted();
                        subscriber.unsubscribe();
                    }
                });
            }
        });
    }

    public void closeSocket() {
        try {
            if (mWebSocket != null) {
                mWebSocket.close(1000, "");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(final long userId, final String text) {
        writeExecutor.execute(new Runnable() {
            @Override
            public void run() {
                JSONObject jso = new JSONObject();
                try {
                    jso.put("initialID", userId);
                    jso.put("sum", text);
                    jso.put("isPayment", true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                RequestBody body = RequestBody.create(WebSocket.TEXT, jso.toString());
                try {
                    mWebSocket.sendMessage(body);
                    getView().clearEdit();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
