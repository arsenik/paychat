package me.paychat.presenter;

import me.paychat.view.IView;

/**
 * Created by a.troshin on 04.07.16.
 */
public interface IPresenter<V extends IView> {
    public void attachView(V IView);
    public void detachView();
}
