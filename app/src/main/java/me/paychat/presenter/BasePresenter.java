package me.paychat.presenter;

import me.paychat.view.IView;

/**
 * Created by a.troshin on 04.07.16.
 */
public class BasePresenter<T extends IView> implements IPresenter<T> {

    private T mView;

    @Override
    public void attachView(T view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    public boolean isViewAttached() {
        return mView != null;
    }

    public T getView() {
        return mView;
    }
}
