package me.paychat.presenter;

import android.content.Context;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

import me.paychat.model.CreateDialog;
import me.paychat.model.DialogInfo;
import me.paychat.model.UserInfo;
import me.paychat.network.PayChatApi;
import me.paychat.util.UserUtil;
import me.paychat.view.DialogsView;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a.troshin on 04.07.16.
 */
public class DialogsPresenter extends BasePresenter<DialogsView> {

    private Subscription mSubscription;

    @Override
    public void attachView(DialogsView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void loadDialogs(Context context) {
        getView().showProgress();
        mSubscription = PayChatApi.api().getByPhoneNumber(UserUtil.currentUserPhone(context))
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<UserInfo>>() {
                    @Override
                    public void onCompleted() {
                        getView().hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<UserInfo> userInfoResponse) {
                        List<DialogInfo> dialogs = Arrays.asList(userInfoResponse.body().dialogs);
                        Log.d("loadDialogs", "size: " + dialogs.size());
                        getView().showDialogs(dialogs);
                    }
                });
    }

    public void logout(Context context) {
        UserUtil.clearUser(context);
        getView().showLogin();
    }

    public void createDialog(final Context context, String myPhone, String contactPhone) {
//        JSONArray phones = new JSONArray();
//        phones.put(myPhone);
//        phones.put(contactPhone);

        mSubscription = PayChatApi.api().createForUsers(myPhone, contactPhone)
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<CreateDialog>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<CreateDialog> createDialogResponse) {
                        loadDialogs(context);
                    }
                });
    }
}
