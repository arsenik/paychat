package me.paychat.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import me.paychat.model.UserInfo;

/**
 * Created by a.troshin on 05.07.16.
 */
public class UserUtil {

    public static final String USER_INFO = "userInfo";

    public static final String ID = "id";
    public static final String PHONE = "phone";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
    }

    public static void saveLoggedUser(Context context, UserInfo userInfo) {
        getPreferences(context).edit()
                .putLong(ID, userInfo.id)
                .putString(PHONE, userInfo.phoneNumber)
                .commit();
    }

    public static String currentUserPhone(Context context) {
        return getPreferences(context).getString(PHONE, "");
    }

    public static boolean loggedIn(Context context) {
        return currentUserId(context) != 0;
    }

    public static long currentUserId(Context context) {
        return getPreferences(context).getLong(ID, 0);
    }

    //TODO call on logout
    public static void clearUser(Context context) {
        getPreferences(context).edit().remove(PHONE).remove(ID).commit();
    }
}
