package me.paychat.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Created by a.troshin on 05.07.16.
 */
public class ContactsUtil {

    public static String getPhone(Context context, Intent intent) {
        if (intent == null) {
            return null;
        }

        Uri contactUri = intent.getData();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor cursor = null;
        String phone = null;
        try {
            cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
            if (cursor.moveToFirst()) {
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                phone = cursor.getString(column);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return phone;
    }
}
