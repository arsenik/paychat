package me.paychat.model;

/**
 * Created by a.troshin on 05.07.16.
 */
public class Transaction {
    public long id;
    public long sum;
    public long initialID;
    public boolean isPayment;
}
