package me.paychat.model;

/**
 * Created by a.troshin on 05.07.16.
 */
public class DialogInfo {

    public long id;

    public Transaction[] transactions;

    public String[] usersList;

    public DialogInfo(long id) {
        this.id = id;
    }
}
