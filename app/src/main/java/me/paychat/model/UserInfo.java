package me.paychat.model;

/**
 * Created by a.troshin on 04.07.16.
 */
public class UserInfo {

    public long id;

    public String phoneNumber;

    public DialogInfo[] dialogs;

    public UserInfo() {
    }
}
