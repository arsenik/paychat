package me.paychat.presenter;

import android.content.Context;
import android.util.Log;

import me.paychat.model.UserInfo;
import me.paychat.network.PayChatApi;
import me.paychat.util.UserUtil;
import me.paychat.view.LoginView;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by a.troshin on 04.07.16.
 */
public class LoginPresenter extends BasePresenter<LoginView> {

    public static final int NO_CONTENT_204 = 204;

    private Subscription mSubscription;

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void checkLoggedIn(Context context) {
        if (UserUtil.loggedIn(context)) {
            getView().next();
        } else {
            getView().hideProgress();
        }
    }

    public void login(final Context context, final String phone) {
        getView().showProgress();
        mSubscription = PayChatApi.api().getByPhoneNumber(phone)
                .switchMap(new Func1<Response<UserInfo>, Observable<Response<UserInfo>>>() {
                    @Override
                    public Observable<Response<UserInfo>> call(
                            Response<UserInfo> userInfoResponse) {
                        Log.d("loginResponse", "code: " + userInfoResponse.code());
                        if (userInfoResponse.code() == NO_CONTENT_204) {
                            return PayChatApi.api().usersCreate(phone);
                        } else {
                            Log.d("loginResponse", "user id: " + userInfoResponse.body().id);
                            UserInfo userInfo = userInfoResponse.body();
                            UserUtil.saveLoggedUser(context, userInfo);
                            Log.d("saveLoggedUser", "id: "+userInfo.id +" phoneNumber: " + userInfo.phoneNumber);
                            return Observable.empty();
                        }
                    }
                }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<UserInfo>>() {
                    @Override
                    public void onCompleted() {
                        Log.d("loginResponse", "onCompleted");
                        getView().next();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("loginResponse", "onError " + e.getMessage());
                        getView().hideProgress();
                        getView().showError(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<UserInfo> userCreateResponse) {
                        Log.d("loginResponse", "onNext");
                        UserInfo userInfo = userCreateResponse.body();
                        UserUtil.saveLoggedUser(context, userInfo);
                        Log.d("saveLoggedUser", "id: "+userInfo.id +" phoneNumber: " + userInfo.phoneNumber);
                    }
                });

    }
}
